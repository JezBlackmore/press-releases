

/*  const url = './pressRelease.json';   */
  const url = 'https://blog-json.s3.eu-west-2.amazonaws.com/pressRelease.json'; 

let filteredArray = [];
let articles = [];
let filteredArticles = [];

const loadJson = () => {
    fetch(url , {
        method: 'GET', 
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        }})
        .then(response => response.json())
        .then(articlesArray => {
            filteredArticles = [...articlesArray];
            filteredArray = [...articlesArray];
            articles = [...articlesArray];
           
            createArticle(articlesArray);
            populateSelector(); 
        })
        .catch(error => console.log(error))
}

loadJson();


let currentPageNumber = 1;

const articleContainer = document.querySelector(".articleContainer");
const categorySelector = document.querySelector(".categorySelector");
const loadMore = document.querySelector('#loadMoreArticles');
const displayNumberOfResults = document.querySelector('#displayNumberOfResults');
const searchArticles = document.querySelector("#searchArticles");

const createArticle = (array) => {

    const pagination = array.slice(0, currentPageNumber * 5);

    if(pagination.length < currentPageNumber * 5){
        loadMore.classList.add('displayNone');
    } else if(array.length == currentPageNumber * 5){
        loadMore.classList.add('displayNone');
    }else{
        loadMore.classList.remove('displayNone');
    }
  
    getNumberOfResults(pagination, array);

    pagination.forEach((article, index) => {

        let articleCard = document.createElement('div');
        articleCard.classList.add('articleCard');
        articleContainer.appendChild(articleCard);

        let articleContainerContent = document.createElement('div');
        articleContainerContent.classList.add('articleContainer__content');

        articleContainerContent.innerHTML = `<h2>${article.title}</h2>${article.intro}<a href=${article.url} class="btn btn-default btn-success">${article.callToAction}</a>`;

        let articleContainerImage = document.createElement('div');
        articleContainerImage.classList.add('articleContainer__image');

        articleContainerImage.innerHTML = `<a href=${article.url}><img src=${article.image}></a>`;

        if(index % 2 === 0){
            articleCard.appendChild(articleContainerContent);
            articleCard.appendChild(articleContainerImage);
        }else{
            articleCard.appendChild(articleContainerImage);
            articleCard.appendChild(articleContainerContent);
        }

    })
}

/* const populateSelector = () => {
    const getValues = articles.reduce((acc, cur) => {
        if(!acc.includes(cur.category)){
            acc.push(cur.category)
        }

        return acc;
    }, [])

    let html = ``;

    getValues.forEach(value => {
        html += `<option value=${value}>${value}</option>`;
    })

    categorySelector.innerHTML = `<span>Filter PRESS RELEASEs:</span> <select name="categories" id="categories"><option value='showAll'>Show All</option>${html}</select>`;

    const categories = document.querySelector('#categories');

    categories.addEventListener('change', (e) => {

        const valueToFilter = e.target.value;
        searchArticles.value = "";
        currentPageNumber = 1;
        loadFilteredArticles(articles, valueToFilter);
    })

} */

const loadFilteredArticles = (array, valueToFilter) => {

    if(valueToFilter !== "showAll"){
        filteredArray = array.filter(article => {
            return article.category === valueToFilter;
        });

        filteredArticles = [...filteredArray];
        articleContainer.innerHTML = ``;
        currentPageNumber = 1;
        createArticle(filteredArray);
    } else{
        currentPageNumber = 1;
        articleContainer.innerHTML = ``;
        filteredArticles = [...articles];

        createArticle(articles);
    }
};



searchArticles.addEventListener("input", (e) => {

    if(e.target.value === "" || e.target.value === null){
        currentPageNumber = 1;
        articleContainer.innerHTML = ``;
        createArticle(articles);

    } else{
        filteredArticles =  articles.filter(article => {
            if(article.title.toLocaleLowerCase().includes(e.target.value.toLocaleLowerCase()) || article.intro.toLocaleLowerCase().includes(e.target.value.toLocaleLowerCase())){
                return true;
            }
        })
    
        if(filteredArticles.length < 1){
            getNumberOfResults([], []);
            currentPageNumber = 1;
            articleContainer.innerHTML = `<h2>No Press Releases Match Your Search.</h2>`;
        } else{
            articleContainer.innerHTML = ``;
            currentPageNumber = 1;
            createArticle(filteredArticles);
        }
    }
})

const getNumberOfResults = (pagination, arrayTotal) => {
    displayNumberOfResults.innerHTML = `${pagination.length} of ${arrayTotal.length}`
}

loadMore.addEventListener('click', () => {
    articleContainer.innerHTML = ``;
    currentPageNumber++;
    createArticle(filteredArticles);
})

